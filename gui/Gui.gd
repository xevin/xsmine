extends CanvasLayer



onready var wood_label = $Inventory/WoodControl/WoodCountLabel
onready var coin_label = $Inventory/CoinsControl/CoinsCountLabel

signal joystick

func _ready():
	update_wood()
	update_coins()
	update_version(Global.VERSION)
	Global.connect("update_resource", self, "_on_update_resources")


func _on_update_resources(type, count):
	match type:
		"wood":
			update_wood()
		"coins":
			update_coins()


func _input(event):
	if event is InputEventScreenTouch:
		show_joystick()
		if event.position.y >= $BottomGuiPosition2D.position.y:
			$Control/VirtualJoystick.global_position = event.position


func _on_VirtualJoystick_analogic_chage(move):
	emit_signal("joystick", move)


func hide_joystick():
	$Control/VirtualJoystick.modulate = Color(1,1,1,0)


func show_joystick():
	$Control/VirtualJoystick.modulate = Color(1,1,1,0.5)


func update_wood():
	wood_label.text = str(Global.user_score.wood)


func update_coins():
	coin_label.text = str(Global.user_score.coins)


func update_version(val):
	$Control2/VersionLabel.text = "v" + val


func _on_TextureButton_pressed():
	OS.window_fullscreen = !OS.window_fullscreen
