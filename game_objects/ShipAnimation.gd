extends Spatial


signal ship_here
signal ship_gone


func start():
	prints("ship animation activate")
	$AnimationPlayer.play("incoming")


func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "incoming":
		$Timer.start()
		emit_signal("ship_here")
	elif anim_name == "outgoing":
		$AnimationPlayer.play("RESET")
		emit_signal("ship_gone")


func _on_Timer_timeout():
	$AnimationPlayer.play("outgoing")
