extends Spatial


export var required_count = 30
export var required_resource = "wood"
export (PackedScene) var UnloadingAnimation

var unloading_anim

signal full
signal resource_taken

var _count = 0

onready var resource_item = $ResourceItem


enum State {
	WAIT_RESOURCE,
	UNLOADING
}
var _state

func _set_state(state):
	_state = state
	match _state:
		State.WAIT_RESOURCE:
			pass
		State.UNLOADING:
			pass


func _ready():
	_set_state(State.WAIT_RESOURCE)
	_update_label()

	unloading_anim = UnloadingAnimation.instance()
	add_child(unloading_anim)
	unloading_anim.connect("ship_here", self, "_resource_taken")
	unloading_anim.connect("ship_gone", self, "_unloading_success")
	unloading_anim.transform = $Position3D.transform


func _resource_taken():
	prints("resource taken")
	_count = 0
	_update_label()
	emit_signal("resource_taken", required_resource, required_count)


func _unloading_success():
	unlock()


func _get_need_count():
	return required_count - _count


func _update_label():
	var text = str(_count) + "/" + str(required_count)
	resource_item.init(required_resource, text)


func change_resource():
	_update_label()


func reset():
	_count = 0
	_update_label()


func unlock():
	_set_state(State.WAIT_RESOURCE)


func activate():
	prints("Pierce activate")


func deactivate():
	prints("Pierce deactivate")


func add_resource(count):
	var need_count = _get_need_count()
	var sub_count = 0

	prints(1)
	if _state == State.UNLOADING:
		return 0

	prints(2)
	if need_count >= count:
		prints(3)
		_count += count
		_update_label()
		sub_count = count
	else:
		prints(4)
		_count += need_count
		_update_label()
		sub_count = need_count

	prints(5)
	if _count == required_count:
		prints(6)
		_set_state(State.UNLOADING)
		unloading_anim.start()
		emit_signal("full", required_resource, required_count)

	return sub_count
