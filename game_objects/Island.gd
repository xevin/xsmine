extends Spatial


signal resource_taken
signal ship_started

var _type = ""
var _loaded_count = 0


func _ready():
	_type = $Pierce.required_resource


func _on_Pierce_full(type, count):
	_loaded_count = count
	$ShipAnimation.start()
	emit_signal("ship_started")


func _on_ShipAnimation_ship_here():
	$Pierce.reset()
	emit_signal("resource_taken", _type, _loaded_count)


func _on_ShipAnimation_ship_gone():
	$Pierce.unlock()
