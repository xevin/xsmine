extends Spatial

export (PackedScene) var particle

# очередь задач (queue of tasks)
var _queue = []


func _add_task(task):
	_queue.push_back(task)
	if _queue.size() == 1:
		# Только что добавили первую задачу
		_run_task(_queue[0])


func _remove_task():
	var end_task = _queue.pop_front()
	remove_child(end_task["instance"])


func _on_task_complete():
	_remove_task()
	if _queue.size() > 0:
		_run_task(_queue[0])


func _run_task(task):
	var instance = task["instance"]
	instance.show()
	var new_pos = Vector3(instance.translation)
	new_pos.y += 4

	var text = ""
	if task["count"] > 0:
		text = "+" + str(task["count"])
	else:
		text = str(task["count"])

	instance.init(task["type"], text)
	$Tween.interpolate_property(
		instance,
		"translation", 
		instance.translation,
		new_pos,
		0.8,
		Tween.TRANS_LINEAR,
		Tween.EASE_IN
	)
	$Tween.start()
	task["running"] = true
	$Timer.start()


func emit(pos, type, count):
	var res = particle.instance()
	res.translation = pos
	add_child(res)
	res.hide()

	_add_task({
		"type": type, 
		"count": count, 
		"pos": pos,
		"instance": res,
		"running": false
	})


func _on_Timer_timeout():
	for t in _queue:
		if !t["running"]:
			_run_task(t)
			break


func _on_Tween_tween_all_completed():
	_on_task_complete()
