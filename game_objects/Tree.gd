extends Spatial

export var wood_count: int = 5
export var level: int = 1
export var type: String = "wood"

signal damage_taken
signal resource_end
signal resource_ready


enum State {
	IDLE,
	READY,
	TAKE_DAMAGE,
	EMPTY
}

onready var _cur_count = wood_count
onready var particles = $CPUParticles

var _state = State.READY
var _queue_state = null
var _animation_run = false


func _set_state(state):
	match state:
		State.READY:
			_cur_count = wood_count
			$tree.show()
			$StaticBody/CollisionShape.disabled = false
			emit_signal("resource_ready", self)
			_state = State.READY
		State.TAKE_DAMAGE:
			_state = State.TAKE_DAMAGE
		State.EMPTY:
			_state = State.EMPTY
			$tree.hide()
			$StaticBody/CollisionShape.disabled = true
			$AnimationPlayer.play("Empty")
			$Timer.start()
		State.IDLE:
			$AnimationPlayer.play("RESET")


func _ready():
	_cur_count = wood_count
	_set_state(State.IDLE)


func is_empty() -> bool:
	return _state == State.EMPTY


func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "TakeDamage":
		if _cur_count > 0:
			_cur_count -= 1
			emit_signal("damage_taken", type, level)

		if _cur_count > 0:
			particles.emitting = true
			_set_state(State.TAKE_DAMAGE)
		else:
			particles.emitting = false
			emit_signal("resource_end")
			_set_state(State.EMPTY)

	if anim_name == "Grow":
		_animation_run = false
		_set_state(State.READY)
		if _queue_state:
			_set_state(_queue_state)
			_queue_state = null


func _on_Timer_timeout():
	$AnimationPlayer.play("Grow")
	_animation_run = true
	_set_state(State.READY)


func take_damage(val):
	prints("tree take_damage", val)
	$AnimationPlayer.play("TakeDamage")


func activate():
	if _animation_run:
		_queue_state = State.TAKE_DAMAGE
	else:
		_set_state(State.TAKE_DAMAGE)


func deactivate():
	particles.emitting = false
	if _state == State.EMPTY:
		return

	if _animation_run:
		_queue_state = State.IDLE
	else:
		_set_state(State.IDLE)
