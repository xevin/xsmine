extends KinematicBody

enum State {
	IDLE,
	WALK,
	CHOP,
	STOP_WALK
}

export var speed = 6
export var fall_acceleration = 70

signal enter_to_area
signal exit_from_area
signal chop_end


onready var ap = $AnimationPlayer

var velocity = Vector3.ZERO
var is_walking = false
var active_zone = null
var direction = Vector3.ZERO
var joystick_direction = Vector3.ZERO

var _state = State.IDLE

var _can_mine = false


func _ready():
	Gui.connect("joystick", self, "_on_joystick_move")


func _set_state(state):
	_state = state
	
	match _state:
		State.IDLE:
			_reset_walk_animation()
			ap.play("Idle")
		State.WALK:
			ap.play("Walk")
		State.CHOP:
			_reset_walk_animation()
			ap.play("Chop")
		_:
			_reset_walk_animation()
			ap.play("Idle")


func _reset_walk_animation():
	if ap.current_animation == "Walk":
		ap.seek(0, true)


func _physics_process(delta):
	direction = Vector3.ZERO

	_get_input()

	if joystick_direction != Vector3.ZERO:
		direction = joystick_direction

	if direction != Vector3.ZERO:
		_set_state(State.WALK)
		direction = direction.normalized()
		var new_transform = $Pivot.transform.looking_at(direction, Vector3.UP)
		$Pivot.transform  = $Pivot.transform.interpolate_with(new_transform, 20 * delta)
	else:
		if _can_mine:
			_set_state(State.CHOP)
		else:
			_set_state(State.IDLE)

	velocity.x = direction.x * speed * delta
	velocity.z = direction.z * speed * delta
	velocity.y = 0

	move_and_collide(velocity)


func _on_joystick_move(move):
	joystick_direction = Vector3(move.x, 0, move.y)


func _get_input():
	var is_input = false
	if Input.is_action_pressed("ui_right"):
		direction.x += 1
		is_input = true
	if Input.is_action_pressed("ui_left"):
		direction.x -= 1
		is_input = true
	if Input.is_action_pressed("ui_down"):
		direction.z += 1
		is_input = true
	if Input.is_action_pressed("ui_up"):
		direction.z -= 1
		is_input = true

	if is_input:
		Gui.hide_joystick()


func _on_Area_area_entered(area):
	emit_signal("enter_to_area", area.get_parent())


func _on_Area_area_exited(area):
	emit_signal("exit_from_area", area.get_parent())


func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "Chop":
		emit_signal("chop_end")


func can_mine(val):
	_can_mine = val
