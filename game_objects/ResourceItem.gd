extends Spatial

onready var label = $Resource/Label3D
onready var icon_coin = $Resource/coin
onready var icon_wood = $Resource/wood
onready var icon = $Resource


func _show_resource(type):
	match type:
		"wood":
			icon_wood.show()
			icon_coin.hide()
		"coins":
			icon_coin.show()
			icon_wood.hide()
		"coin":
			icon_coin.show()
			icon_wood.hide()


func init(type, text):
	_show_resource(type)
	label.text = str(text)
