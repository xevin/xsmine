extends Spatial

var _cur_area = null

export (PackedScene) var resource_indicator

onready var player = $Player
onready var resource_emitter = $ResourceEmitter


func _ready():
	resource_indicator.instance()
	Global.connect("update_resource", self, "_on_update_resource")
	$InterpolatedCamera.set_target($Player/Camera)
	$InterpolatedCamera.translation = $Player/Camera.global_translation


func _on_PlayerKinematicBody_enter_to_area(area):
	_cur_area = area

	if area.is_in_group("resource"):
		if !area.is_empty():
			player.can_mine(true)
			area.activate()
	elif area.is_in_group("shop"):
		area.activate()
		var required_resource = area.required_resource
		var sub_count = area.add_resource(Global.user_score[required_resource])
		prints("sub_count", sub_count, Global.user_score[required_resource])
		Global.sub_resource(required_resource, sub_count)


func _on_PlayerKinematicBody_exit_from_area(area):
	_cur_area.deactivate()
	player.can_mine(false)
	_cur_area = null


func _on_Tree_damage_taken(type, level):
	Global.add_wood(1)


func _on_Tree_resource_end():
	player.can_mine(false)


func _on_Tree_resource_ready(area):
	if _cur_area and _cur_area == area and area.is_in_group("resource"):
		player.can_mine(true)
		area.activate()


func _on_update_resource(resource_type, count):
	var pos = Vector3(player.translation)
	pos.y += 5
	resource_emitter.emit(pos, resource_type, count)


func _resource_taken(type, count):
	var coins = Global.resource_to_coins(type, count)
	Global.add_coins(coins)


func _on_PlayerKinematicBody_chop_end():
	if _cur_area.is_in_group("resource"):
		_cur_area.take_damage(Global.user_score["axe"])


func _on_Pierce_full():
	prints("pierce full")
