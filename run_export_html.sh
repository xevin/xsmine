#!/usr/bin/env sh

GODOT_BINARY=Godot_v3.5.3-stable_win64.exe 
DIST_COUNT=`ls -1 dist | wc -l`
BUILD_VER=`printf "%04d" $((DIST_COUNT+1))`
BUILD_NAME=`echo build-$BUILD_VER.zip`

function auto_scale_patch() {
	# Для Godot v3

	# Этот патч нужен для автомасштабирования канваса под размер экрана
	# Перед применением, в настройках проекта надо указать
	# 1) window/size/resizable
	# 2) window/dpi/allow_hidpi
	# 3) window/stretch/mode = disabled
	# 4) window/stretch/aspect = ignore
	#
	# В шаблоне экспорта Export HTML5
	# html/canvas_resize_policy = Project
	#
	# и добавить стили в шаблон экспорта
	# canvas { position: absolute; top: 0; left: 50%; transition: -50% }
	
	# Так-же обрати внимание на innerWidth / 16 и innerHeight / 9 в патче
	# это соотношение сторон в который будет сохраняться при изменении размера экрана
	echo "Apply Autoscale Patch"
	sed -i "s/\}const scale=GodotDisplayScreen\.getPixelRatio();/\}let scale=null;if \(Math\.floor\(window\.innerWidth \/ 16) < Math\.floor(window\.innerHeight \/ 9\)\) \{scale=\(width\/window\.innerWidth\)+0.01;\} else \{scale=\(height\/window\.innerHeight\)+0.01\};/" build/index.js
}

function patch_instant_bridge_shell() {
	# Убираем какой-то мусор из собранной html-страницы
	echo "Fix InstantBridge shell"
	sed -i '/^\ \ \ })();/,$d' index.html
}

function zip_build() {
	echo Zipping $BUILD_NAME
	powershell Compress-Archive -Path ".\*" $BUILD_NAME 
}


$GODOT_BINARY --export "HTML5" build/index.html

auto_scale_patch

cd build

patch_instant_bridge_shell

zip_build

mv $BUILD_NAME ../dist
