extends Node

const VERSION = "0.2.0"

signal update_resource

var EVENTS = {
	"first_ship": true
}

var user_score = {
	"wood": 65,
	"coins": 0,
	"axe": 1
}

var RESOURCE_PRICE = {
	"wood": {
		"count": 10,
		"price": 1
	}
}

func add_resource(resource, count):
	user_score[resource] += count
	if count:
		emit_signal("update_resource", resource, count)


func sub_resource(resource, count):
	user_score[resource] -= count
	if count:
		emit_signal("update_resource", resource, -count)


func add_wood(count):
	add_resource("wood", count)


func sub_wood(count):
	sub_resource("wood", count)


func add_coins(count):
	add_resource("coins", count)


func sub_coins(count):
	sub_resource("coins", count)


func resource_to_coins(type, count):
	var table = RESOURCE_PRICE[type]

	var price = (count / table.count) * table.price
	return price
